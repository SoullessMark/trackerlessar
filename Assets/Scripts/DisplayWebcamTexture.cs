﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayWebcamTexture : MonoBehaviour {
    public GameObject plane;
    public GameObject cube;
    
	// Use this for initialization
	void Start () {



        /*if (Application.isMobilePlatform)
        {
            GameObject cameraParent = new GameObject("cameraParent");
            cameraParent.transform.position = this.gameObject.transform.position;
            this.transform.parent = cameraParent.transform;
            cameraParent.transform.Rotate(Vector3.right, 90f);
        }*/

        Input.gyro.enabled = true;

        WebCamTexture webCamText = new WebCamTexture();
        plane.gameObject.GetComponent<MeshRenderer>().material.mainTexture = webCamText;
        webCamText.Play();

        StartCoroutine(setObjects());
	}

    // Update is called once per frame
    void Update()
    {
        Quaternion webCamRotation = new Quaternion(Input.gyro.attitude.x, Input.gyro.attitude.y, -Input.gyro.attitude.z, -Input.gyro.attitude.w);
        this.transform.localRotation = webCamRotation;
    }
    IEnumerator setObjects()
    {
        yield return new WaitForSeconds(1.75f);
        cube.transform.parent = null;
    }
}
